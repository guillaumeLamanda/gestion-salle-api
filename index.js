import express from "express"
import bodyParser from "body-parser"
import logger from "morgan"
import cors from "cors"
import configureRoute from "./routes"

const app = express()
app.use(logger("dev"))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  exposedHeaders: ["Content-Range", "X-Content-Range", "X-Total-Count"]
}
app.use(cors(corsOptions))

configureRoute(app)

app.use(function(error, req, res, next) {
  res.status(500).json({ error })
})

app.listen("3000")
