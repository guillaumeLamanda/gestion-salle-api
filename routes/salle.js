import { Salle } from "../db"

export default app => {
  app
    .route("/salle/:id")
    .get(async (req, res, next) => {
      return res.json(await Salle.findByPk(req.params.id).catch(e => next(e)))
    })
    .put(async (req, res) => {
      return res.json(
        await (await Salle.findByPk(req.params.id)).update(req.body)
      )
    })
    .delete(async (req, res) => {
      return res.json(await (await Salle.findByPk(req.params.id)).destroy())
    })

  app
    .route("/salle")
    .get(async (req, res) => {
      const { rows: salles, count } = await Salle.findAndCountAll()
      res.set("X-Total-Count", count)
      return res.json(salles)
    })
    .post(async (req, res) => res.json(await Salle.create(req.body)))
}
