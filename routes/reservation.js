import { Reservation } from "../db"

export default app => {
  app
    .route("/reservation/:id")
    .get(async (req, res) => {
      return res.json(
        await Reservation.findByPk(req.params.id, { include: [{ all: true }] })
      )
    })
    .put(async (req, res) => {
      return res.json(
        await (await Reservation.findByPk(req.params.id)).update(req.body, {
          include: [{ all: true }]
        })
      )
    })
    .delete(async (req, res) => {
      return res.json(
        await (await Reservation.findByPk(req.params.id)).destroy()
      )
    })

  app
    .route("/reservation")
    .get(async (req, res) => {
      const { rows: reservations, count } = await Reservation.findAndCountAll()
      res.set("X-Total-Count", count)
      return res.json(reservations)
    })
    .post(async (req, res, next) =>
      res.json(
        await Reservation.create(req.body, { include: [{ all: true }] }).catch(
          e => next(e)
        )
      )
    )
}
