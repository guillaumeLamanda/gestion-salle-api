import { Utilisateur } from "../db"

export default app => {
  app
    .route("/utilisateur/:id")
    .get(async (req, res) => {
      return res.json(await Utilisateur.findByPk(req.params.id))
    })
    .put(async (req, res) => {
      return res.json(
        await (await Utilisateur.findByPk(req.params.id)).update(req.body)
      )
    })
    .delete(async (req, res) => {
      return res.json(
        await (await Utilisateur.findByPk(req.params.id)).destroy()
      )
    })

  app
    .route("/utilisateur")
    .get(async (req, res) => {
      const { rows: users, count } = await Utilisateur.findAndCountAll()
      res.set("X-Total-Count", count)
      return res.json(users)
    })
    .post(async (req, res) => res.json(await Utilisateur.create(req.body)))
}
