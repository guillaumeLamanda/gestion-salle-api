import salle from "./salle"
import utilisateurs from "./utilisateurs"
import reservation from "./reservation"

const configureRoute = app => {
  salle(app)
  utilisateurs(app)
  reservation(app)
}

export default configureRoute
