import Sequelize from "sequelize"

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "db.sqlite",
  logging: false
})

export const Salle = sequelize.define("Salle", {
  nom: {
    type: Sequelize.STRING,
    unique: true
  },
  type: Sequelize.STRING
})

/**
 * @type Reservation
 * salle
 * utilisateur
 * debut
 * fin
 */
export const Reservation = sequelize.define("Reservation", {
  debut: Sequelize.DATE,
  fin: Sequelize.DATE
})

/**
 * @type ModelUtilisateur
 * nom
 * prenom
 */
export const Utilisateur = sequelize.define("Utilisateur", {
  nom: Sequelize.STRING,
  prenom: Sequelize.STRING
})

Reservation.belongsTo(Salle, {
  foreignKey: {
    allowNull: false
  }
})
Salle.hasMany(Reservation, { onDelete: "cascade" })

Reservation.belongsTo(Utilisateur, {
  foreignKey: {
    allowNull: false
  }
})
Utilisateur.hasMany(Reservation, { onDelete: "cascade" })
;(async () => await sequelize.sync())()

export default sequelize
